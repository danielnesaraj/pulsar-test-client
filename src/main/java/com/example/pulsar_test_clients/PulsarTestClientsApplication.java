package com.example.pulsar_test_clients;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PulsarTestClientsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PulsarTestClientsApplication.class, args);
	}

}
