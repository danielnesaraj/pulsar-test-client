package com.example.pulsar_test_clients;

import org.apache.pulsar.client.api.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.concurrent.TimeUnit;

@Configuration
@EnableScheduling
public class PulsarConfig {
    private final String pulsarClientUrl;

    public PulsarConfig(@Value("${app.pulsar.service.url}") String pulsarClientUrl) {
        this.pulsarClientUrl = pulsarClientUrl;
    }

    @Bean
    public PulsarClient pulsarClient() throws PulsarClientException {
        return PulsarClient.builder()
                .serviceUrl(pulsarClientUrl)
                .operationTimeout(5, TimeUnit.SECONDS)
                .build();
    }

    @Bean
    public Producer<byte[]> pulsarProducer(PulsarClient pulsarClient,
                                           @Value("${app.pulsar.topic}") String topic) throws PulsarClientException {
        return pulsarClient
                .newProducer(Schema.BYTES)
                .topic(topic)
                .create();
    }

    @Bean
    public Consumer<byte[]> pulsarConsumer(PulsarClient pulsarClient, @Value("${app.pulsar.topic}") String topic,
                                           @Value("${app.pulsar.subscription}") String subscription, PulsarListener pulsarListener)
            throws PulsarClientException {
        return pulsarClient
                .newConsumer(Schema.BYTES)
                .topic(topic)
                .subscriptionName(subscription)
                .subscriptionType(SubscriptionType.Shared)
                .receiverQueueSize(100)
                .messageListener(pulsarListener)
                .negativeAckRedeliveryDelay(5, TimeUnit.MINUTES)
                .subscribe();
    }
}
