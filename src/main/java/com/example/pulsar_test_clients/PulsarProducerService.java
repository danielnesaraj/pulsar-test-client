package com.example.pulsar_test_clients;

import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.client.api.Schema;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Set;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

@Service
public class PulsarProducerService {
    private static final String message = "This is a String";

    private final Producer<byte[]> pulsarProducer;
    private final AtomicInteger id = new AtomicInteger(0);
    private final int maxCount;
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();
    private final Set<Integer> errors = new ConcurrentSkipListSet<>();

    public PulsarProducerService(Producer<byte[]> pulsarProducer,
                                 @Value("${app.message.max.count}") int maxCount) {
        this.pulsarProducer = pulsarProducer;
        this.maxCount = maxCount;
    }

    @Scheduled(fixedRate = 100)
    public void publishMessage() {
        // hack to stop after count
        if (this.id.get() >= maxCount) {
            return;
        }
        int currentId = this.id.incrementAndGet();

        CompletableFuture.runAsync(new Sender(pulsarProducer, currentId), executorService)
                .exceptionally(trackFailure());
    }

    @Scheduled(fixedDelay = 2, timeUnit = TimeUnit.MINUTES, initialDelay = 3)
    public void retryPublishMessage() {
        if (errors.isEmpty()) {
            return;
        } else {
            System.out.println("Retrying " + errors.size() + " failures!");
        }

        Iterator<Integer> errorIterator = errors.iterator();
        while (errorIterator.hasNext()) {
            Integer error = errorIterator.next();
            errors.remove(error);
            CompletableFuture.runAsync(new Sender(pulsarProducer, error), executorService)
                    .exceptionally(trackFailure());
        }
    }

    private Function<Throwable, Void> trackFailure() {
        return t -> {
            try {
                String[] split = t.getMessage().split(" ");
                String id = split[split.length - 1];
                errors.add(Integer.valueOf(id));
                System.out.println("Tracked message failure for : " + id);
            } catch (Exception e) {
                System.err.println("Error tracking failure on publish!");
                e.printStackTrace(System.err);
            }
            return null;
        };
    }

    public static class Sender implements Runnable {
        private final Producer<byte[]> pulsarProducer;
        private final int id;

        public Sender(Producer<byte[]> pulsarProducer, int id) {
            this.pulsarProducer = pulsarProducer;
            this.id = id;
        }

        @Override
        public void run() {
            try {
                pulsarProducer.newMessage(Schema.BYTES)
                        .value(PulsarProducerService.message.getBytes(StandardCharsets.UTF_8))
                        .property("id", String.valueOf(id))
                        .send();
                System.out.println("Producer sent:" + message + " " + id);
            } catch (PulsarClientException e) {
                throw new RuntimeException(String.valueOf(id), e);
            }
        }
    }
}
