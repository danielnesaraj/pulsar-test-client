package com.example.pulsar_test_clients;

import org.apache.pulsar.client.api.Consumer;
import org.apache.pulsar.client.api.Message;
import org.apache.pulsar.client.api.MessageListener;
import org.apache.pulsar.client.api.PulsarClientException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Component
public class PulsarListener implements MessageListener<byte[]> {
    private final Set<Integer> ids = Collections.synchronizedSet(new HashSet<Integer>());
    private final int maxCount;

    public PulsarListener(@Value("${app.message.max.count}") int maxCount) {
        this.maxCount = maxCount;
    }

    @Override
    public void received(Consumer<byte[]> consumer, Message<byte[]> message) {
        int id = Integer.parseInt(message.getProperty("id"));
        ids.add(id);
        System.out.println("Consumer received: " + new String(message.getValue(), StandardCharsets.UTF_8) + " " + id);
        if (ids.size() == maxCount) {
            System.out.println("RECEIVE COMPLETE!");
        }
        try {
            consumer.acknowledge(message);
        } catch (PulsarClientException e) {
            System.err.println("Error while acknowledging!");
            e.printStackTrace(System.err);
            throw new RuntimeException(e);
        }
    }
}
