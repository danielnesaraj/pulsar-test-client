# Getting Started

## Setup pulsar
Use minikube and pulsar and setup a cluster.

## Setup a tunnel
Once the cluster is up, and the proxy is running, open a tunnel for minikube:
```shell
minikube tunnel
```

## Setup topic
Run the following in toolset pod:
```shell
(
bin/pulsar-admin tenants create example
bin/pulsar-admin namespaces create example/test
bin/pulsar-admin namespaces set-auto-topic-creation example/test -d
bin/pulsar-admin namespaces set-retention example/test -t 1h -s 1G
bin/pulsar-admin namespaces set-message-ttl example/test -ttl 259200
bin/pulsar-admin topics create-partitioned-topic persistent://example/test/test-topic -p 5
bin/pulsar-admin topics create-subscription -s test-subscription persistent://example/test/test-topic
)
```